#pragma once

#include "stdafx.h"
#include "PhysVector.hpp"

#include "material.h"
#include "visualitzacio.h"

class SceneModel
{
public:
	SceneModel():vertex(NULL),normal(NULL),colors(NULL),texture(NULL),material(1),vboId(80) { };
	SceneModel(GLfloat* vertex_vit, GLfloat* normal_vit, GLfloat* colors_vit, GLint* texture_vit,
		int material_vit, GLint vboId_vit) :
		vertex(vertex_vit), normal(normal_vit), colors(colors_vit), texture(texture_vit),
		material(material_vit), vboId(vboId_vit) {};
	SceneModel(GLint* texture_vit,int material_vit,GLint vboId_vit):
		vertex(NULL), normal(NULL), colors(NULL), texture(texture_vit),
		material(material_vit), vboId(vboId_vit) {};

	SceneModel& operator=(const SceneModel& Smodel);

	void create_sphere(double radius);
	void create_cube(double length);
	void create_board(GLdouble m_scaleX_vit, GLdouble m_scaleY_vit, GLdouble m_scaleZ_vit, PhysVector<GLdouble> coord_vit);
	void transform(PhysVector<GLdouble> coord_vit, GLdouble m_angleX_vit, GLdouble m_angleY_vit, GLdouble m_angleZ_vit,
		GLdouble m_scaleX_vit, GLdouble m_scaleY_vit, GLdouble m_scaleZ_vit);
	void transform(GLdouble coord_vitx, GLdouble coord_vity, GLdouble coord_vitz, GLdouble m_angleX_vit, GLdouble m_angleY_vit, GLdouble m_angleZ_vit,
		GLdouble m_scaleX_vit, GLdouble m_scaleY_vit, GLdouble m_scaleZ_vit);
	bool load_model();//faltantexturas
	void push_gpu();
private:
	GLfloat* vertex;
	GLfloat* normal;
	GLfloat* colors;
	GLint* texture;
	int material;
	GLint vboId;
};

