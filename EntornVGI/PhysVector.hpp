#pragma once

#include <math.h>
#include <assert.h>
#include <stdio.h>

template <class dType>
class PhysVector
{
public:
	// Constructores
	PhysVector(int size);
	PhysVector(dType* data, int size);

	// Destructor (duh)
	~PhysVector();

	// Operadores
	PhysVector<dType>& operator=(const PhysVector<dType> &vec);
	PhysVector<dType> operator+(const PhysVector<dType> &sumand);
	PhysVector<dType> operator-(const PhysVector<dType> &sumand);
	dType operator*(const PhysVector<dType> &other);
	PhysVector<dType> operator*(dType scalar);

	dType operator[](int index);

	// Métodos
	dType modulus();
	dType distance(const PhysVector<dType> &other);
	dType scalarprod(const PhysVector<dType> &other);
	void normalize();
	void absolute();
	
	void print_vector();

private:
	int m_size;
	dType* m_data;
};

template <class dType>
PhysVector<dType>::PhysVector(int size)
{
	if (size > 0)
	{
		m_size = size;
		m_data = new dType[size];

		for (int i = 0; i < m_size; i++)
			m_data[i] = (dType) 0.0;
	}
}

template <class dType>
PhysVector<dType>::PhysVector(dType* data, int size)
{
	if (size > 0 && data != nullptr)
	{
		m_size = size;
		m_data = new dType[size];

		for (int i = 0; i < size; i++)
			m_data[i] = data[i];
	}
}

template <class dType>
PhysVector<dType>::~PhysVector()
{
	delete[] m_data;
}

template <class dType>
PhysVector<dType>& PhysVector<dType>::operator=(const PhysVector<dType> &vec)
{
	if (&vec != this)
	{
		if (m_size != vec.m_size || m_data == nullptr)
		{
			m_data = new dType[m_size];
			m_size = vec.m_size;
		}
		for (int i = 0; i < m_size; i++)
			m_data[i] = vec.m_data[i];
	}
	return *this;
}

template <class dType>
PhysVector<dType> PhysVector<dType>::operator+(const PhysVector<dType>& sumand)
{
	assert(sumand.m_size == m_size);
	dType* content = new dType[m_size];
	for (int i = 0; i < m_size; i++)
		content[i] = sumand.m_data[i] + m_data[i];

	return PhysVector<dType>(content, m_size);
}

template <class dType>
PhysVector<dType> PhysVector<dType>::operator-(const PhysVector<dType>& sumand)
{
	assert(sumand.m_size == m_size);
	dType* content = new dType[m_size];
	for (int i = 0; i < m_size; i++)
		content[i] = m_data[i] - sumand.m_data[i];

	return PhysVector<dType>(content, m_size);
}

template <class dType>
PhysVector<dType> PhysVector<dType>::operator*(dType scalar)
{
	dType* content = new dType[m_size];
	for (int i = 0; i < m_size; i++)
		content[i] = m_data[i] * scalar;

	return PhysVector<dType>(content, m_size);
}

template <class dType>
inline dType PhysVector<dType>::operator[](int index)
{
	assert(m_data != nullptr);
	assert(m_size > 0);

	return m_data[index];
}

template <class dType>
dType PhysVector<dType>::modulus()
{
	assert(m_data != nullptr);
	assert(m_size > 0);

	dType sum = (dType)(0.0);

	for (int i = 0; i < m_size; i++)
		sum += (m_data[i] * m_data[i]);

	return sqrt(sum);
}

template <class dType>
void PhysVector<dType>::normalize()
{
	assert(m_data != nullptr);
	assert(m_size > 0);
	
	dType modul = (*this).modulus();
	
	for (int i = 0; i < m_size; i++)
	{
		m_data[i] /= modul;
	}
}

template <class dType>
dType PhysVector<dType>::distance(const PhysVector<dType>& other)
{
	return ((*this) - other).modulus();
}

template <class dType>
void PhysVector<dType>::absolute()
{
	assert(m_data != nullptr);
	assert(m_size > 0);

	for (int i = 0; i < m_size; i++)
		if (m_data[i] < (dType)(0))
			m_data[i] = -m_data[i];
}

template <class dType>
dType PhysVector<dType>::operator*(const PhysVector<dType>& other)
{
	// Este es correcto
	assert(m_data != nullptr);
	assert(m_size > 0);

	// El otro es correcto
	assert(other.m_data != nullptr);
	assert(other.m_size > 0);

	// Mismo tamaño
	assert(m_size == other.m_size);

	dType result = (dType)0.0;
	for (int i = 0; i < m_size; i++)
		result += other.m_data[i] * other.m_data[i];

	return result;
}

template <class dType>
dType PhysVector<dType>::scalarprod(const PhysVector<dType>& other)
{
	// Este es correcto
	assert(m_data != nullptr);
	assert(m_size > 0);

	// El otro es correcto
	assert(other.m_data != nullptr);
	assert(other.m_size > 0);

	// Mismo tamaño
	assert(m_size == other.m_size);

	dType result = (dType)0.0;
	for (int i = 0; i < m_size; i++)
		result += other.m_data[i] * other.m_data[i];

	return result;
}

template <class dType>
void PhysVector<dType>::print_vector()
{
	printf("[");
	for(int i = 0; i < m_size; i++)
	{
		printf("%f ", m_data[i]);
	}
	printf("]\n");
}