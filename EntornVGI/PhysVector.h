#pragma once

#include <math.h>

template <class dType>
class PhysVector
{
public:
	// Constructores
	PhysVector(int size);
	PhysVector(dType* data, int size);
	PhysVector(PhysVector<dType> other);

	// Destructor (duh)
	~PhysVector();

	// Operadores
	PhysVector<dType> operator+(PhysVector<dType> sumand);
	PhysVector<dType> operator-(PhysVector<dType> sumand);
	dType operator*(PhysVector<dType> other);
	PhysVector<dType> operator*(dType scalar);
	
	dType operator[](int index);

	// M�todos
	dType modulus();
	dType distance(PhysVector<dType> other);
	dType scalarprod(PhysVector<dType> other);
	void normalize();
	void absolute();

private:
	int m_size;
	dType* m_data;
};

template <class dType>
PhysVector<dType>::PhysVector(int size)
{
	if (size > 0)
	{
		m_size = size;
		m_data = new dType[size];

		for (int i = 0; i < m_size; i++)
			m_data[i] = 0;
	}
}

template <class dType>
PhysVector<dType>::PhysVector(dType* data, int size)
{
	if (size > 0 && data != nullptr)
	{
		m_size = size;
		m_data = new dType[size];

		for (int i = 0; i < size; i++)
			m_data[i] = data[i];
	}
}

template <class dType>
PhysVector<dType>::~PhysVector()
{
	delete m_data[];
}

template <class dType>
PhysVector<dType> PhysVector<dType>::operator+(PhysVector<dType> sumand)
{
	assert(sumand.m_size == m_size);
	dType content = new content[m_size];
	for (int i = 0; i < m_size; i++)
		content[i] = sumand.m_data[i] + m_data[i];

	return PhysVector(content, m_size);
}

template <class dType>
PhysVector<dType> PhysVector<dType>::operator-(PhysVector<dType> sumand)
{
	assert(sumand.m_size == m_size);
	dType content = new content[m_size];
	for (int i = 0; i < m_size; i++)
		content[i] = m_data[i] - sumand.m_data[i];

	return PhysVector(content, m_size);
}

template <class dType>
PhysVector<dType> PhysVector<dType>::operator*(dType scalar)
{
	dType content = new content[m_size];
	for (int i = 0; i < m_size; i++)
		content[i] = m_data[i] * scalar;

	return PhysVector(content, m_size);
}

template <class dType>
inline dType PhysVector<dType>::operator[](int index)
{
	assert(m_data != nullptr);
	assert(m_size > 0);

	return m_data[index];
}

template <class dType>
dType PhysVector<dType>::modulus()
{
	assert(m_data != nullptr);
	assert(m_size > 0);

	dType sum = (dType)(0.0);

	for (int i = 0; i < m_size; i++)
		sum += (m_data * m_data);

	return sqrt(sum);
}

template <class dType>
void PhysVector<dType>::normalize()
{
	(*this) * 1 / this->modulus();
}

template <class dType>
dType PhysVector<dType>::distance(PhysVector<dType> other)
{
	return ((*this) - other).modulus();
}

template <class dType>
void PhysVector<dType>::absolute()
{
	assert(m_data != nullptr);
	assert(m_size > 0);

	for (int i = 0; i < m_size; i++)
		if (m_data[i] < (dType)(0))
			m_data[i] = -m_data[i];
}

template <class dType>
dType PhysVector<dType>::operator*(PhysVector<dType> other)
{
	// Este es correcto
	assert(m_data != nullptr);
	assert(m_size > 0);

	// El otro es correcto
	assert(other.m_data != nullptr);
	assert(other.m_size > 0);

	// Mismo tama�o
	assert(m_size == other.m_size);

	dType result = (dType)0.0;
	for (int i = 0; i < m_size; i++)
		result += other.m_data[i] * other.m_data[i];

	return result;
}

template <class dType>
dType PhysVector<dType>::scalarprod(PhysVector<dType> other)
{
	// Este es correcto
	assert(m_data != nullptr);
	assert(m_size > 0);

	// El otro es correcto
	assert(other.m_data != nullptr);
	assert(other.m_size > 0);

	// Mismo tama�o
	assert(m_size == other.m_size);

	dType result = (dType)0.0;
	for (int i = 0; i < m_size; i++)
		result += other.m_data[i] * other.m_data[i];

	return result;
}