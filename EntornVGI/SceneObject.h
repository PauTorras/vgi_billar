#pragma once

#include "stdafx.h"
#include "PhysVector.hpp"
#include "SceneModel.h"

class SceneObject
{
public:
	// Constructores
	SceneObject();

	// Destructores


	// Metodos virtuales
	virtual void draw();
	virtual void update();
	virtual GLdouble distance(SceneObject object);

	// Metodos
	void displace(PhysVector<GLdouble> newpos);
	void rotate(GLdouble varX, GLdouble varY, GLdouble varZ);
protected:
	

private:
	// Coordenadas de la posici�n
	PhysVector<GLdouble> coord;

	// Angulos de rotaci�n seg�n los ejes
	GLdouble m_angleX;
	GLdouble m_angleY;
	GLdouble m_angleZ;

	// Coeficientes de escala
	GLdouble m_scaleX;
	GLdouble m_scaleY;
	GLdouble m_scaleZ;

	// Modelo 3D
	SceneModel m_model;
};

