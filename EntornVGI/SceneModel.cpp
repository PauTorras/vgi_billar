#include "stdafx.h"
#include "SceneModel.h"

SceneModel& SceneModel::operator=(const SceneModel& Smodel)
{

	vertex=Smodel.vertex;
	normal=Smodel.normal;
	colors=Smodel.colors;
	texture=Smodel.texture;
	material=Smodel.material;
	vboId=Smodel.vboId;
	return *this;
}

void SceneModel::create_sphere(double radius)
{
	gluSphere(radius, vboId,vboId);
	
}

void SceneModel::create_cube(double length)
{
	glutSolidCube(length);
}

void SceneModel::create_board(GLdouble m_scaleX_vit, GLdouble m_scaleY_vit, GLdouble m_scaleZ_vit,PhysVector<GLdouble> coord_vit)
{
	glDisable(GL_TEXTURE_2D);

	glTranslatef(coord_vit[0], coord_vit[1], coord_vit[2]);
	//textura de la base
	if (texture != NULL)
	{
		GLfloat sPlane[4] = { 0.0f, 0.0f, 0.00f, 0.0f };
		GLfloat tPlane[4] = { 0.00f, 0.00f, 0.0f, 0.00f };
		// Activar textura planxa
		glBindTexture(GL_TEXTURE_2D, texture[0]);

		glTexGenfv(GL_S, GL_OBJECT_PLANE, sPlane);
		glTexGenfv(GL_T, GL_OBJECT_PLANE, tPlane);

		glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
		glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		glEnable(GL_TEXTURE_2D);
	}

	//labase
	glPushMatrix();
	transform(coord_vit,0.0f, 0.0f, 0.0f,m_scaleX_vit,m_scaleY_vit,0.5);
	create_cube(1);
	glPopMatrix();
	//textura de las paredes
	if (texture != NULL)
	{
		GLfloat sPlane[4] = { 0.05f, 0.0f, 0.00f, 0.250f };
		GLfloat tPlane[4] = { 0.00f, 0.05f, 0.1f, 0.00f };
		// Activar textura planxa
		glBindTexture(GL_TEXTURE_2D, texture[0]);

		glTexGenfv(GL_S, GL_OBJECT_PLANE, sPlane);
		glTexGenfv(GL_T, GL_OBJECT_PLANE, tPlane);

		glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
		glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

		glEnable(GL_TEXTURE_2D);
	}
	//pared ezquierda
	glPushMatrix();
	transform((coord_vit[0] - m_scaleX_vit - 2), coord_vit[1], coord_vit[2], 0.0f, 0.0f, 0.0f,4, m_scaleY_vit,m_scaleZ_vit);
	create_cube(1);
	glPopMatrix();

	//pared derecha
	glPushMatrix();
	transform(coord_vit[0] + m_scaleX_vit + 2, coord_vit[1], coord_vit[2], 0.0f, 0.0f, 0.0f, 4, m_scaleY_vit, m_scaleZ_vit);
	create_cube(1);
	glPopMatrix();

	//pared delantera
	glPushMatrix();				
	transform(coord_vit[0], coord_vit[1] - m_scaleY_vit - 2, coord_vit[2], 0.0f, 0.0f, 0.0f, m_scaleX_vit,4, m_scaleZ_vit);
	create_cube(1);
	glPopMatrix();

	//pared trasera
	glPushMatrix();
	transform(coord_vit[0], coord_vit[1] + m_scaleY_vit + 2, coord_vit[2], 0.0f, 0.0f, 0.0f, m_scaleX_vit, 4, m_scaleZ_vit);
	create_cube(1);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
	//comprovar un huevo de cosas
	
}

void SceneModel::transform(PhysVector<GLdouble> coord_vit, GLdouble m_angleX_vit, GLdouble m_angleY_vit, GLdouble m_angleZ_vit, GLdouble m_scaleX_vit, GLdouble m_scaleY_vit, GLdouble m_scaleZ_vit)
{
	glScaled(m_scaleX_vit, m_scaleY_vit, m_scaleZ_vit);
	glTranslated(coord_vit[0], coord_vit[1], coord_vit[2]);
	glRotated(m_angleX_vit, 1,0, 0);
	glRotated(m_angleY_vit, 0,1,0);
	glRotated(m_angleZ_vit, 0,0,1);


}

void SceneModel::transform(GLdouble coord_vitx, GLdouble coord_vity, GLdouble coord_vitz, GLdouble m_angleX_vit, GLdouble m_angleY_vit, GLdouble m_angleZ_vit, GLdouble m_scaleX_vit, GLdouble m_scaleY_vit, GLdouble m_scaleZ_vit)
{
	glScaled(m_scaleX_vit, m_scaleY_vit, m_scaleZ_vit);
	glTranslated(coord_vitx, coord_vity, coord_vitz);
	glRotated(m_angleX_vit, 1, 0, 0);
	glRotated(m_angleY_vit, 0, 1, 0);
	glRotated(m_angleZ_vit, 0, 0, 1);
}

bool SceneModel::load_model()
{

	if (texture != NULL) SetTextureParameters(texture[0], true, true, true, false);
	if (texture != NULL)glEnable(GL_TEXTURE_2D);
	else glDisable(GL_TEXTURE_2D);

	// Textura 
	if (texture != NULL)
	{
		CColor ccolor_voit;
		ccolor_voit.a = 1;
		ccolor_voit.r = 1;
		ccolor_voit.g = 1;
		ccolor_voit.b = 1;
		bool sw_mater[4] = { false, true, true, true };
		SeleccionaColor(true, sw_mater, ccolor_voit);
		//ver codenadas
	}
	if (colors != NULL) glColor4f(colors[0], colors[1], colors[2], 1.0f);
	
	glPushMatrix();
	
	return true;
}

void SceneModel::push_gpu()
{
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}
